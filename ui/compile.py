#!/usr/bin/env python
# coding=utf-8

import os
import sys

class Colored(object):
    RED = '\033[31m'       # 红色
    GREEN = '\033[32m'     # 绿色
    YELLOW = '\033[33m'    # 黄色
    BLUE = '\033[34m'      # 蓝色
    FUCHSIA = '\033[35m'   # 紫红色
    CYAN = '\033[36m'      # 青蓝色
    WHITE = '\033[37m'     # 白色

    #: no color
    RESET = '\033[0m'      # 终端默认颜色

    def color_str(self, color, s):
        return '{}{}{}'.format(
            getattr(self, color),
            s,
            self.RESET
        )

    def red(self, s):
        return self.color_str('RED', s)

    def green(self, s):
        return self.color_str('GREEN', s)

    def yellow(self, s):
        return self.color_str('YELLOW', s)

    def blue(self, s):
        return self.color_str('BLUE', s)

    def fuchsia(self, s):
        return self.color_str('FUCHSIA', s)

    def cyan(self, s):
        return self.color_str('CYAN', s)

    def white(self, s):
        return self.color_str('WHITE', s)

color = Colored()

def compile_all():
    """
    Compile dialog.ui pyuic5
    """
    files = os.listdir()
    for file in files:
        if str(file).endswith('.ui'):
            name = file.split('.')[0]
            print("%s\t%-25s -> %-20s"\
                  %(color.yellow('Compiling for PyQt5: '),color.red('%s.ui'%name),color.green('%s.py'%name)))
            os.system("pyuic5 -o %s.py %s.ui"%(name,name))

        if str(file).endswith('.qrc'):
            name = file.split('.')[0]
            print("%s\t%-25s -> %-20s"\
                  %(color.yellow('Compiling for PyQt5: '),color.red('%s.qrc'%name),color.green('%s.py'%name)))
            os.system("pyrcc5 -o %s.py %s.qrc"%(name,name))





if __name__ == "__main__":
    if sys.argv[1].endswith('.ui'):
        name = sys.argv[1].replace('.ui','')
        print("%s\t%-25s -> %-20s"\
                %(color.yellow('Compiling for PyQt5: '),color.red('%s.ui'%name),color.green('%s.py'%name)))
        os.system("pyuic5 -o %s.py %s.ui"%(name,name))
    elif sys.argv[1].endswith('.qrc'):
        name = sys.argv[1].replace('.qrc','')
        print("%s\t%-25s -> %-20s"\
                %(color.yellow('Compiling for PyQt5: '),color.red('%s.qrc'%name),color.green('%s.py'%name)))
        os.system("pyrcc5 -o %s.py %s.qrc"%(name,name))
    else:
        compile_all()
