# fsc-system

#### 介绍

辐射场红外人员目标识别跟踪。

#### 软件架构

1. 当前版本采用Yolov5-v4.0版本进行训练数据

#### 安装教程

1. git clone 
2. 搭建yolov5-v4.0版本环境

#### 使用说明


#### 问题记录及解决

1. 使用yolov5框架时，离线情况下运行会出现如下问题：

```
requests.exceptions.ConnectionError: HTTPSConnectionPool(host='api.github.com', port=443): Max retries exceeded with url: /repos/ultralytics/yolov5/releases/latest (Caused by NewConnectionError('<urllib3.connection.HTTPSConnection object at 0x000002912ADFC130>: Failed to establish a new connection: [Errno 11001] getaddrinfo failed'))`
```

**原因**：yolov5在加载本地模型时，会从GitHub仓库下的release远程获取训练好的权重文件。
**解决方法**：关闭远程获取权重文件。 找到`yolov5/models/experimental.py`文件，在文件中找到函数名为`attempt_load`的函数，如下：

```
def attempt_load(weights, map_location=None):
    # Loads an ensemble of models weights=[a,b,c] or a single model weights=[a] or weights=a
    model = Ensemble()
    for w in weights if isinstance(weights, list) else [weights]:
        attempt_download(w)
        model.append(torch.load(w, map_location=map_location)['model'].float().fuse().eval())  # load FP32 model

    # Compatibility updates
    for m in model.modules():
        if type(m) in [nn.Hardswish, nn.LeakyReLU, nn.ReLU, nn.ReLU6]:
            m.inplace = True  # pytorch 1.7.0 compatibility
        elif type(m) is Conv:
            m._non_persistent_buffers_set = set()  # pytorch 1.6.0 compatibility

    if len(model) == 1:
        return model[-1]  # return model
    else:
        print('Ensemble created with %s\n' % weights)
        for k in ['names', 'stride']:
            setattr(model, k, getattr(model[-1], k))
        return model  # return ensemble

```
注释掉`attempt_download(w)`这一行，关闭远程获取和下载权重文件。


#### 训练数据命令日志

**训练数据, 参考yolov5-v4.0版本**

进入yolov5目录
`cd yolov5`

**2021-08-23**
1. `python train.py --weights ./weights/yolov5s.pt --cfg ./models/yolov5s.yaml --data ./data/hongwai-1/data.yaml --batch-size 24 --device 0`
2. `python train.py --weights ./weights/best-1.pt --cfg ./models/yolov5s.yaml --data ./data/hongwai-2/data.yaml --batch-size 24 --device 0`
3. `python detect.py --weights ./runs/train/exp51/weights/best-2.pt `


**2021-08-23**
1. `python train.py --weights ./weights/yolov5m.pt --cfg ./models/yolov5m.yaml --data ./data/hongwai-1/data.yaml --batch-size 24 --device 0`
2. `python train.py --weights ./weights/best-1.pt --cfg ./models/yolov5m.yaml --data ./data/hongwai-2/data.yaml --batch-size 24 --device 0`
3. `python detect.py --weights ./runs/train/exp51/weights/best-2.pt `

**2021-09-01**
1. `python train.py --weights ./weights/yolov5m.pt --cfg ./models/yolov5m.yaml --data ./data/hongwai-1/data.yaml --batch-size 24 --device 0`
2. `python train.py --weights ./weights/best-1.pt --cfg ./models/yolov5m.yaml --data ./data/hongwai-2/data.yaml --batch-size 24 --device 0`
3. `python detect.py --weights ./weights/yolov5m-infrared-2021-08-23-02.pt  --source data/hongwai-09-01`


**2021-09-26**

- 多人训练
1. `python train.py --weights ./weights/yolov5m-infrared-2021-08-23-02.pt --cfg ./models/yolov5m.yaml --data ./data/hongwai-duoren/data.yaml --batch-size 24 --device 0`

- 测试结果
2. `python detect.py --weights ./weights/yolov5m-infrared-2021-09-26-01.pt  --source data/hongwai-09-01/c3/2`

**2021-09-27**
1. `python train.py --weights ./weights/yolov5m.pt --cfg ./models/yolov5m.yaml --data ./data/hongwai-all-01/data.yaml --batch-size 24 --device 0`
2. `python detect.py --weights ./weights/yolov5m-infrared-2021-09-27-01.pt  --source data/hongwai-09-01/c3/2`

3. `python train.py --weights ./weights/yolov5m.pt --cfg ./models/yolov5m.yaml --data ./data/hongwai-all-02/data.yaml --batch-size 24 --device 0`
4. `python detect.py --weights ./weights/yolov5m-infrared-2021-09-27-02.pt  --source data/hongwai-09-01/c3/2`