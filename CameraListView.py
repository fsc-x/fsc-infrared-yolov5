#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""
Created on 2018年8月4日
@author: Irony
@site: https://pyqt.site , https://github.com/PyQt5
@email: 892768447@qq.com
@file: QListView.显示自定义Widget
@description:
"""

try:
    from PyQt5.QtCore import QSize
    from PyQt5.QtGui import QStandardItemModel, QStandardItem
    from PyQt5.QtWidgets import QListView, QWidget, QHBoxLayout, QLineEdit, \
        QPushButton, QApplication, QLabel, QSpinBox
except ImportError:
    from PySide2.QtCore import QSize
    from PySide2.QtGui import QStandardItemModel, QStandardItem
    from PySide2.QtWidgets import QListView, QWidget, QHBoxLayout, QLineEdit, \
        QPushButton, QApplication

from person import PersonWidget
import os

class CustomWidget(QWidget):

    person_widget = None
    is_open = False

    def __init__(self, context={}, *args, **kwargs):
        super(CustomWidget, self).__init__(*args, **kwargs)
        layout = QHBoxLayout(self)
        layout.setContentsMargins(0, 0, 0, 0)
        name = context['name'] if 'name' in context else 'None'
        resize = context['resize'] if 'resize' in context else 1

        self.spinBox_resize = QSpinBox()
        self.spinBox_resize.setValue(resize)
        self.spinBox_resize.setRange(1, 5)
        self.spinBox_resize.valueChanged.connect(self.resizeChanged)

        self.pushButton_open = QPushButton("开启")

        self.label_status = QLabel("无人")

        layout.addWidget(QLabel(name, self))
        layout.addWidget(QLabel('缩放比例:', self))
        layout.addWidget(self.spinBox_resize)
        layout.addWidget(self.pushButton_open)
        layout.addWidget(self.label_status)

        self.person_widget = PersonWidget()
        self.person_widget.set_opt(source=name, weights='%s/yolov5/weights/yolov5s-infrared-2021-09-28-01.pt' % os.getcwd())
        self.person_widget.signal_person_num.connect(self.personTotal)

        self.pushButton_open.clicked.connect(self.openCamera)

    def sizeHint(self):
        # 决定item的高度
        return QSize(200, 40)

    def resizeChanged(self, value):
        self.person_widget.camera_resize = value

    def openCamera(self):
        if self.person_widget.is_open:
            self.pushButton_open.setText("开启")
            self.person_widget.stop()
        else:
            self.pushButton_open.setText("关闭")
            self.person_widget.start()

    def personTotal(self, n):
        self.label_status.setText('person: %s' % n)


class CameraListView(QListView):
    items = []

    def __init__(self, *args, **kwargs):
        super(CameraListView, self).__init__(*args, **kwargs)
        # 模型
        self._model = QStandardItemModel(self)
        self.setModel(self._model)

    def addCamera(self, camera={}):
        item = QStandardItem()
        self._model.appendRow(item)  # 添加item

        # 得到索引
        index = self._model.indexFromItem(item)
        widget = CustomWidget(context=camera)
        item.setSizeHint(widget.sizeHint())  # 主要是调整item的高度

        # 设置自定义的widget
        self.setIndexWidget(index, widget)
        self.items.append(widget)

    def clearCamera(self):
        self._model.clear()
        self.items = []

    def update(self, cameras=[]):
        self._model.clear()
        # 循环生成10个自定义控件
        for c in cameras:
            item = QStandardItem()
            self._model.appendRow(item)  # 添加item

            # 得到索引
            index = self._model.indexFromItem(item)
            widget = CustomWidget(context=c)
            item.setSizeHint(widget.sizeHint())  # 主要是调整item的高度
            # 设置自定义的widget
            self.setIndexWidget(index, widget)
            self.items.append(widget)


if __name__ == '__main__':
    import sys

    app = QApplication(sys.argv)
    w = CameraListView()
    w.show()
    sys.exit(app.exec_())
